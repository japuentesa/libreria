<?php

require_once "conexion.php";

class Modeloformularios{

    static public function mdlRegistro($tabla,$datos){

$stmt=Conexion::conectar()->prepare("INSERT INTO $tabla(Nombre,Correo, Clave) VALUES (:Nombre,:Correo, :Clave)");

$stmt->bindParam(":Nombre",$datos["Nombre"],PDO::PARAM_STR);
$stmt->bindParam(":Correo",$datos["Correo"],PDO::PARAM_STR);
$stmt->bindParam(":Clave",$datos["Clave"],PDO::PARAM_STR);

if($stmt->execute()){

    return "ok";
}else{

    print_r(Conexion::conectar()->errorInfo());

}
    
$stmt->close();

} 
static public function mdlSeleccionarRegistros($tabla, $item, $valor){

    if ($item == null && $valor == null){

    $stmt=Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY id DESC");
    
    
    $stmt->execute();
    
        return $stmt->fetchAll();

    }else{  

        $stmt=Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item=:$item ORDER BY id DESC");
        $stmt->bindParam(":".$item , $valor, PDO::PARAM_STR);

    
    
        $stmt->execute();
        
            return $stmt->fetch();

    }
        
    $stmt->close();
    
    

    }

}

?>