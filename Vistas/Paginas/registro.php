<div class="text-center"><h2><p class="text-muted">REGISTRO</p>
</h2></div>
<div class="d-flex justify-content-center">

<form class="p-5 bg-light" method="post">
<form action="/action_page.php">

<div class="form-group">
<label for="nombre">Nombre: </label>

<div class="input-group">
<div class="input-group-prepend">
<span class="input-group-text"><i class="fas fa-user"></i></span>

</div>

<input type="text" class="form-control" placeholder="Ingresar Nombre" id="nombre" name="registronombre">
</div>


<div class="form-group">
<label for="email">Correo Electronico</label>
<div class="input-group">
<div class="input-group-prepend">
<span class="input-group-text"><i class="fas fa-at"></i></span>

</div>

<input type="email" class="form-control" placeholder="Ingresar Correo" id="email" name="registroemail">
</div>
<div class="form-group">
<label for="pwd">Clave</label>
<div class="input-group">
<div class="input-group-prepend">
<span class="input-group-text"><i class="fas fa-unlock-alt"></i></span>

</div>

<input type="pwd" class="form-control" placeholder="Ingresar Clave" id="pwd" name="registroclave">
</div>
<button type="submit" class="btn btn-primary">Enviar</button>
</form>
</div>

<?php

$registro = FormulariosControlador::ctrRegistro();
if ($registro =="ok"){

    echo'<script>
    if(window.history.replaceState){
        window.history.replaceState(null,null,window.location.href);

}
</script>';
echo'<div class="alert alert-success">El Usuario Ha sido Registrado</div>';

}

?>


