<?php
if(isset($_SESSION["validarIngreso"])){

  if($_SESSION["validarIngreso"]!= "ok"){

    echo'<script>window.location="index.php?paginas=ingreso";</script>';

    return;


  }

}else{

  echo'<script>window.location="index.php?paginas=ingreso";</script>';

    return;
    
}
$usuarios=FormulariosControlador::ctrSeleccionarRegistros();


?>
<div class="text-center"><h2><p class="text-muted">Personas ingresadas</p>
</h2></div>

        <p class="text-muted">Encontraras las personas ingresadas con su correo y clave en la siguiente tabla:</p>            
        <table class="table table-dark">
          <thead>
            <tr>
            <th>#</th>
              <th>Nombre</th>
              <th>Correo</th>
              <th>Clave</th>
              <th>Acciones</th>
            </tr>
          </thead>

          <tbody>

          <?php foreach($usuarios as $key => $value): ?>

            <tr>
            <td><?php echo ($key+1); ?></td>
              <td><?php echo $value["Nombre"]; ?></td>
              <td><?php echo $value["Correo"]; ?></td>
              <td><?php echo $value["Clave"]; ?></td>
              <td>
              <div class="btn-group">
              <button class="btn btn-warning"><i class="fas fa-pencil-alt"></i></button>
              <button class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
              </div>
              </td>

            </tr>

            <?php endforeach ?>
          
          </tbody>
        </table>